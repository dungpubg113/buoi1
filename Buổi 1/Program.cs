﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buổi_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            char key;
            int i, j;

            do
            {
                Console.Write("Ấn một số trên bàn phím:\n");
                Console.Write("1: Tìm độ dài chuỗi nhập vào\n");
                Console.Write("2: Đếm số từ chuỗi nhập vào\n");
                Console.Write("3: Đếm số nguyên âm và phụ âm của chuỗi\n");
                Console.Write("4: Tính tổng 1 chữ số của số nguyên n\n");
                Console.Write("5: Sắp xếp mảng theo thứ tự giảm dân và in ra mảng mới\n");
                Console.Write("6: Nhập 1 số, inths giai thừa của só đó và xuất ra màn hình\n");
                Console.Write("7: Nhập và xuất ra màn hình thông tin căn cước công dân Viet Nam\n");
                Console.Write("8: Nhập số nguyên dương n và tìm n số Fibonacci\n");

                key = (char)Console.Read();
                switch (key)
                {
                    case '1':
                        /* Khai bao mot chuoi */
                        int l = 0;
                        string a;
                        Console.Write("\nTìm độ dài chuỗi nhập vào:\n");
                        Console.WriteLine("Nhập một chuỗi vào: " + Console.ReadLine());/*Nhập vào 1 chuỗi*/
                        a = Console.ReadLine();
                        /*Đếm độ dài của chuỗi*/
                        foreach (char chr in a)
                        {
                            l += 1;
                        }

                        Console.Write("Độ dài chuỗi là: {0}\n", l);
                        Console.ReadKey();
                        break;
                    case '2':

                        string b; /* Khai bao mot chuoi */
                        int bien_dem, length;

                        Console.Write("\n Đếm số từ trong chuỗi:\n");
                        Console.WriteLine("Nhập một chuỗi vào: " + Console.ReadLine());
                        b = Console.ReadLine();

                        length = 0;
                        bien_dem = 1;

                        /* lặp tới phần cuối của chuỗi */
                        while (length <= b.Length - 1)
                        {
                            /* kiểm tra khoảng trắng giữa các từ */
                            if (b[length] == ' ' || b[length] == '\n' || b[length] == '\t')
                            {
                                bien_dem++;
                            }

                            length++;
                        }

                        Console.Write("Số từ có trên chuỗi là: {0}\n", bien_dem);

                        Console.ReadKey();
                        break;

                    case '3':
                        string c; //khai bao mot chuoi
                        int lenght, nguyen_am, phu_am;

                        Console.Write("\nĐếm số nguyên âm và phụ âm trong chuỗi:\n");
                        Console.WriteLine("Nhập một chuỗi: " + Console.ReadLine());
                        c = Console.ReadLine();

                        nguyen_am = 0;
                        phu_am = 0;
                        lenght = c.Length;
                        /*Lặp tới phần cuối trong độ dài của chuỗi*/
                        for (i = 0; i < lenght; i++)
                        {
                            /*kiểm tra số lượng nguyên âm và phụ âm*/
                            if (c[i] == 'a' || c[i] == 'e' || c[i] == 'i' || c[i] == 'o' ||
                                c[i] == 'u' || c[i] == 'A' || c[i] == 'E' || c[i] == 'I' ||
                                c[i] == 'O' || c[i] == 'U')
                            {
                                nguyen_am++;
                            }
                            else if ((c[i] >= 'a' && c[i] <= 'z') || (c[i] >= 'A' && c[i] <= 'Z'))
                            {
                                phu_am++;
                            }
                        }

                        Console.Write("\nSo nguyen am co trong chuoi la: {0}\n", nguyen_am);
                        Console.Write("So phu am co trong chuoi la: {0}\n", phu_am);

                        break;
                    case '4':

                        int sum = 0;
                        int m;
                        Console.Write("\n Tính tổng các chữ số nguyên n \n");
                        Console.WriteLine("Nhập vào số nguyên n: " + Console.ReadLine());
                        int n = int.Parse(Console.ReadLine());
                        while (n > 0)
                        {
                            m = n % 10;
                            sum = sum + m;
                            n = n / 10;
                        }
                        Console.WriteLine("\nTổng : {0}\n", sum);
                        break;
                    case '5':
                        int[] array = new int[100];
                        int z, tam;
                        Console.Write("\nSắp xếp mảng theo thứ tự giảm dần:\n");
                        Console.WriteLine("Nhập vào số lượng phần tử của mảng: " + Console.ReadLine());
                        z = int.Parse(Console.ReadLine());
                        Console.Write("Nhập {0} phần từ vào trong mảng:\n", z);
                        for (i = 0; i < z; i++)
                        {
                            Console.Write("Phần tử - {0}: ", i);
                            array[i] = Convert.ToInt32(Console.ReadLine());
                        }
                        for (i = 0; i < z; i++)
                        {
                            for (j = i + 1; j < z; j++)
                            {
                                if (array[i] < array[j])
                                {
                                    //cach trao doi gia tri
                                    tam = array[i];
                                    array[i] = array[j];
                                    array[j] = tam;
                                }
                            }
                        }

                        Console.Write("\nIn ra các phần tử với thứ tự giảm dần:\n");
                        for (i = 0; i < z; i++)
                        {
                            Console.Write("{0}  ", array[i]);
                        }
                        Console.Write("\n\n");

                        break;
                    case '6':
                        int x, giai_thua = 1;
                        Console.WriteLine("Nhập vào số giai thừa cần tính:\n" + Console.ReadLine());
                        x = int.Parse(Console.ReadLine());
                        for (i = 1; i <= x; i++)
                        {
                            giai_thua *= i;
                        }

                        Console.WriteLine($"\n {x}! = {giai_thua}\n");
                        break;

                    case '7':

                        ThongTin TT1 = new ThongTin();
                        Console.WriteLine("Nhập vào thông tin CCCD");
                        NhapThongTin(out TT1);
                        Console.ReadLine();
                        Console.WriteLine("Thong tin sinh vien vua nhap la: \n");
                        XuatThongTin(TT1);
                        Console.ReadLine();
                        break;
                    case '8':


                        int v;
                        Console.WriteLine("Nhập vào số phần tử Fibonacci ban muon in: " + Console.ReadLine());
                        v = int.Parse(Console.ReadLine());
                        v -= 1;
                        //Su dung vong lap for de lap den so ma nguoi dung nhap vao
                        Console.Write($"Số Fibonacci thứ n {v + 1} là: {InFibonacci(v)}\n");



                        break;
                    default:
                        Console.WriteLine("Phím bạn ấn không phải là số!");
                        break;
                }
                Console.ReadKey();
            } while (key != 1);
        }
        static int InFibonacci(int num)
        {
            if (num == 0)
                return 0;
            else if (num == 1)
                return 1;
            else
                return (InFibonacci(num - 1) + InFibonacci(num - 2));
        }

        struct ThongTin
        {
            public string HoTen;
            public double MaCCCD;
            public string NgaySinh;
            public string GioiTinh;
            public string QuocTich;
            public string DiaChi;

        }

        static void NhapThongTin(out ThongTin TT)
        {
            Console.WriteLine("Họ và tên:" + Console.ReadLine());
            TT.HoTen = Console.ReadLine();
            Console.Write("Số CCCD:  \n");
            TT.MaCCCD = double.Parse(Console.ReadLine());
            Console.Write("Ngày sinh: ");
            TT.NgaySinh = Console.ReadLine();
            Console.Write("Giới tính: ");
            TT.GioiTinh = Console.ReadLine();
            Console.Write("Quốc tịch: ");
            TT.QuocTich = Console.ReadLine();
            Console.Write("Địa chỉ thường chú: ");
            TT.DiaChi = Console.ReadLine();

        }

        static void XuatThongTin(ThongTin TT)
        {
            Console.WriteLine("Họ tên: " + TT.HoTen);
            Console.WriteLine("Mã CCCD: " + TT.MaCCCD);
            Console.WriteLine("Ngày sinh: " + TT.NgaySinh);
            Console.WriteLine("Giới Tính: " + TT.GioiTinh);
            Console.WriteLine("Quốc tịch: " + TT.QuocTich);
            Console.WriteLine("Địa chỉ thường chứ: " + TT.DiaChi);
        }

    }
}
